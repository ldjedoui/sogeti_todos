package fr.capgemini.todo.app.service;

import fr.capgemini.todo.app.model.Todo;
import fr.capgemini.todo.app.repository.TodosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TodosService {
    private TodosRepository todosRepository;

    @Autowired
    public TodosService(TodosRepository todosRepository) {
        this.todosRepository = todosRepository;
    }

    public List<Todo> getAllTodos() {
        return todosRepository.findAllByOrderByOrdAscIdDesc();
    }

    @Transactional
    public void setTodoState(Long idTodo) {
        this.todosRepository.findById(idTodo).ifPresent(todo -> {
            todo.setState(!todo.getState());
            if (todo.getState()) {
                todo.setOrd(this.todosRepository.getMaxOrd() + 1);
            } else {
                todo.setOrd(0L);
            }
            this.todosRepository.save(todo);
        });
    }

    @Transactional
    public Todo getTodo(Long idTodo) {
        return this.todosRepository.findById(idTodo).orElse(null);
    }

    @Transactional
    public Todo createOrUpdateTodo(Todo todo) {
        return this.todosRepository.save(todo);
    }

    @Transactional
    public void delete(Long idTodo) {
        this.todosRepository.deleteById(idTodo);
    }

    @Transactional
    public void resetTodos() {
        todosRepository.deleteAll();
        todosRepository.save(new Todo(false, "Make the dinner", "Only vegetarian meal."));
        Todo todo = todosRepository.save(new Todo(false, "Wash the dishes", "There is no dishwasher, it has to be done by hand."));
        setTodoState(todo.getId());
        todosRepository.save(new Todo(false, "Walking the dog", "Let him do his stuff before coming back."));
        todosRepository.save(new Todo(false, "Pay the rent", "Has to be done before the 25th of the month."));
        todo = todosRepository.save(new Todo(false, "Prepare Samantha's birthday", "It is a surprise! Don't tell her!"));
        setTodoState(todo.getId());
        todosRepository.save(new Todo(false, "Go to the school show"));
    }
}
