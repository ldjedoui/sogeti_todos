package fr.capgemini.todo.app.repository;

import fr.capgemini.todo.app.model.Todo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TodosRepository extends JpaRepository<Todo, Long> {
    List<Todo> findAllByOrderByOrdAscIdDesc();

    @Query("select max(ord) from Todo")
    Long getMaxOrd();
}
