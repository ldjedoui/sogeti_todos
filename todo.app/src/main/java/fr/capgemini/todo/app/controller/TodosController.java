package fr.capgemini.todo.app.controller;

import fr.capgemini.todo.app.model.Todo;
import fr.capgemini.todo.app.service.TodosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todos")
public class TodosController {
    private TodosService todosService;

    @Autowired
    public TodosController(TodosService todosService) {
        this.todosService = todosService;
    }

    @GetMapping()
    public List<Todo> getAllTodos() {
        return todosService.getAllTodos();
    }

    @PostMapping("{idTodo}")
    public void setTodoState(@PathVariable("idTodo") Long idTodo) {
        todosService.setTodoState(idTodo);
    }

    @GetMapping("{idTodo}")
    public Todo getTodo(@PathVariable("idTodo") Long idTodo) {
        return todosService.getTodo(idTodo);
    }

    @PostMapping()
    public void setTodo(@RequestBody Todo todo) {
        todosService.createOrUpdateTodo(todo);
    }

    @DeleteMapping("{idTodo}")
    public void deleteTodo(@PathVariable("idTodo") Long idTodo) {
        todosService.delete(idTodo);
    }

    @PostMapping("reset")
    public void resetTodos() {
        todosService.resetTodos();
    }
}
