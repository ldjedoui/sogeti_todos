package fr.capgemini.todo.app;

import fr.capgemini.todo.app.model.Todo;
import fr.capgemini.todo.app.service.TodosService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@ContextConfiguration
@Transactional
public class ApplicationTests {

	@Resource
	private TodosService todosService;

	@Test
	public void testGetAllTodos() {
		List<Todo> todos = todosService.getAllTodos();
		assertThat(todos.size()).isEqualTo(0);

		todosService.resetTodos();

		todos = todosService.getAllTodos();
		assertThat(todos.size()).isEqualTo(6);
	}

	@Test
	public void testSetTodoState() {
		todosService.resetTodos();

		List<Todo> todos = todosService.getAllTodos();

		Todo todo = todos.get(0);
		Long id = todo.getId();
		assertThat(todo.getState()).isFalse();

		todosService.setTodoState(todo.getId());
		todos = todosService.getAllTodos();
		todo = todos.get(todos.size()-1);
		assertThat(todo.getId()).isEqualTo(id);
		assertThat(todo.getState()).isTrue();

		todosService.setTodoState(todo.getId());
		todo = todosService.getAllTodos().get(0);
		assertThat(todo.getId()).isEqualTo(id);
		assertThat(todo.getState()).isFalse();

		// There is at least one checked todo
		todos = todosService.getAllTodos();
		assertThat(todos.stream().anyMatch(Todo::getState)).isTrue();

		// Uncheck all the todos
		for (Todo curTodo : todos) {
			if (curTodo.getState()) {
				todosService.setTodoState(curTodo.getId());
			}
		}

		// There is no checked todo
		todos = todosService.getAllTodos();
		assertThat(todos.stream().noneMatch(Todo::getState)).isTrue();

		todo = todos.get(0);
		assertThat(todo.getState()).isFalse();

		todosService.setTodoState(todo.getId());
		todos = todosService.getAllTodos();
		todo = todos.get(todos.size()-1);
		assertThat(todo.getId()).isEqualTo(id);
		assertThat(todo.getState()).isTrue();

		todosService.setTodoState(todo.getId());
		todo = todosService.getAllTodos().get(0);
		assertThat(todo.getId()).isEqualTo(id);
		assertThat(todo.getState()).isFalse();
	}

	@Test
	public void testGetTodo() {
		todosService.resetTodos();
		List<Todo> todos = todosService.getAllTodos();
		Todo todo = todos.get(0);
		Todo todo2 = todosService.getTodo(todo.getId());

		assertThat(todo.getId()).isEqualTo(todo2.getId());
		assertThat(todo.getTitle()).isEqualTo(todo2.getTitle());
		assertThat(todo.getDetail()).isEqualTo(todo2.getDetail());
		assertThat(todo.getState()).isEqualTo(todo2.getState());
	}

	@Test
	public void testCreateOrUpdateTodo() {
		List<Todo> todos = todosService.getAllTodos();
		assertThat(todos.size()).isEqualTo(0);

		Todo todo = new Todo(false, "Todo's title");
		todosService.createOrUpdateTodo(todo);

		todos = todosService.getAllTodos();
		assertThat(todos.size()).isEqualTo(1);

		todos.get(0).setTitle("New title");
		Todo todoReturned = todosService.createOrUpdateTodo(todos.get(0));

		todos = todosService.getAllTodos();
		assertThat(todos.size()).isEqualTo(1);
		Todo todoFromDb = todos.get(0);
		assertThat(todoFromDb.getTitle()).isEqualTo("New title");

		assertThat(todoReturned.getId()).isEqualTo(todoFromDb.getId());
		assertThat(todoReturned.getTitle()).isEqualTo(todoFromDb.getTitle());
		assertThat(todoReturned.getDetail()).isEqualTo(todoFromDb.getDetail());
		assertThat(todoReturned.getState()).isEqualTo(todoFromDb.getState());
	}
}