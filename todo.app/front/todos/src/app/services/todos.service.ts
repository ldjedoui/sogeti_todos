import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Todo} from "../models/todo";
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TodosService {
  todos: Todo[] = [];

  todosSubject: Subject<Todo[]> =  new Subject<Todo[]>();

  constructor(private httpClient: HttpClient) {
  }

  emitTodos() {
    this.todosSubject.next(this.todos);
  }

  getTodos() {
    this.httpClient.get('/api/todos').subscribe(
      (data: any) => {
        this.todos = [];
        data.forEach((e: any) => this.todos.push(Todo.fromJson(e)));
        this.emitTodos();
      },
      (error) => {}
    );
  }

  setTodoState(idTodo: number): Observable<any> {
    return this.httpClient.post('/api/todos/' + idTodo, {});
  }

  getTodo(idTodo: number): Observable<Todo> {
      const url = `/api/todos/${idTodo}`;
    return this.httpClient.get<Todo>(url);
  }

  setTodo(todo: Todo) : Observable<any> {
    return this.httpClient.post('/api/todos/', todo);
  }

  deleteTodo(idTodo: number) {
    this.httpClient.delete('/api/todos/' + idTodo, {}).subscribe(
      (data) => {},
      (error) => {}
    );
  }

  resetTodos() {
    this.todos = [];
    this.httpClient.post('/api/todos/reset', {}).subscribe(
      (data) => {this.getTodos();},
      (error) => {}
    );
  }

}
