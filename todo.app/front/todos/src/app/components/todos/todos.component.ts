import {Component, OnInit} from '@angular/core';
import {TodosService} from "../../services/todos.service";
import {DialogService} from "../../services/dialog.service";


@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss']
})
export class TodosComponent implements OnInit {

  constructor(
    private todosService : TodosService,
    private dialogService: DialogService
  ) { }

  ngOnInit(): void {
  }

  resetTodos(): void {
    this.dialogService.openDialog(
      'Are you sure ?',
      'Are you sure you want to reset todos list ?',
      () => this.todosService.resetTodos()
    );
  }

}
