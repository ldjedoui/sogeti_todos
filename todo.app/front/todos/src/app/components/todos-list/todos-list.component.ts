import { Component, OnDestroy, OnInit } from '@angular/core';
import {TodosService} from "../../services/todos.service";
import {Todo} from "../../models/todo";
import {Subscription} from "rxjs";
import {Router} from "@angular/router";
import {DialogService} from "../../services/dialog.service";

@Component({
  selector: 'app-todos-list',
  templateUrl: './todos-list.component.html',
  styleUrls: ['./todos-list.component.scss']
})
export class TodosListComponent implements OnInit, OnDestroy {
  todos: Todo[] = [];
  todosSubscription: Subscription;

  constructor(
    private todosService : TodosService,
    private router: Router,
    private dialogService: DialogService
  ) {
    this.todosSubscription = this.todosService.todosSubject.subscribe((data) =>  {
      this.todos = data;
    });
  }

  ngOnInit(): void {
    this.todosService.getTodos();
  }

  ngOnDestroy(): void {
    this.todosSubscription.unsubscribe();
  }

  changeState(todo : Todo): void {
    this.todosService.setTodoState(todo.id).subscribe(_ => this.todosService.getTodos());
  }

  goToForm(todoId?: number) {
    const id = todoId ? todoId : 0;
    this.router.navigate(['todo/edit/' + id]);
  }

  goToNewForm() {
    this.router.navigate(['todo/new']);
  }

  delete(todoId: number) {
    this.dialogService.openDialog(
      'Are you sure ?',
      'Are you sure you want to delete this todo ?',
      () => {
        this.todosService.deleteTodo(todoId);
        this.todos = this.todos.filter(e => e.id !== todoId);
      }
    );
  }
}
