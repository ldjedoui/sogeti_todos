import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Todo} from "../../models/todo";
import {TodosService} from "../../services/todos.service";

@Component({
  selector: 'app-todo-form',
  templateUrl: './todo-form.component.html',
  styleUrls: ['./todo-form.component.scss']
})
export class TodoFormComponent implements OnInit {
  todoForm: FormGroup;
  submitted = false;
  todo: Todo;

  constructor(
    private todosService : TodosService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router
  ) { }

  ngOnInit(): void {
    const id = parseInt(this.route.snapshot.paramMap.get('id')!, 10);
    if (id) {
      this.todosService.getTodo(id).subscribe(todo => {
        this.todo = todo;
        this.todoForm = this.fb.group({
          id : this.todo.id,
          state : this.todo.state,
          title: this.todo.title,
          detail: this.todo.detail
        });
      });
    } else {
      this.todo = {id : 0, state : false, title: '', detail: ''};
      this.todoForm = this.fb.group({
        id : this.todo.id,
        state : this.todo.state,
        title: this.todo.title,
        detail: this.todo.detail
      });
    }
  }

  // convenience getter for easy access to form fields
  get f() { return this.todoForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.todoForm.invalid) {
      return;
    }

    this.todosService.setTodo(this.todoForm.value).subscribe(
      (_) => this.goToHome()
    );
  }

  onCancel() {
    this.submitted = false;
    this.goToHome();
  }

  goToHome() {
    this.router.navigate(['']);
  }

}
