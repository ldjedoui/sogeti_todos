
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

import {Component, Inject} from '@angular/core';

export interface DialogData {
  title: string;
  message: string;
}

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent {
  result: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) {}
}
