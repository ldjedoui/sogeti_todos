import {required} from "@rxweb/reactive-form-validators";

export class Todo {
  constructor(
    @required()
    public id: number,
    public state: boolean,
    public title: string,
    public detail: string
  ) {
  }

  static fromJson(obj: any): Todo {
    return new Todo(
      obj.id,
      obj.state,
      obj.title,
      obj.detail
    );
  }
}
