import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {TodosComponent} from "./components/todos/todos.component";
import {TodoFormComponent} from "./components/todo-form/todo-form.component";

const routes: Routes = [
  { path: '', component: TodosComponent },
  { path: 'todo/edit/:id', component: TodoFormComponent },
  { path: 'todo/new', component: TodoFormComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
